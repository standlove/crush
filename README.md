# Culture Works CRUSH Rating - MS Teams Scheduled Meetings Bot PoC

This file is the deployment guide for: Culture Works CRUSH Rating - MS Teams Scheduled Meetings Bot PoC.

## Dependencies
- NodeJS https://nodejs.org/en/ (v8+)
- MongoDB https://www.mongodb.com/ (v3.4+)

## Configuration
Configuration is at `config/default.js`, you may also set env variables.
There are following config params:
- `port`: the app's listen port
- `microsoftAppId`: the app (bot)'s id
- `microsoftAppPassword`: the app's secret key
- `mongodbURI`: the mongodb uri
- `oauth`: the configuration about the oauth 2.
- `oauth.authURL`: the entry point for oauth.
- `oauth.callbackURL`: the callback url of the authorization.
- `oauth.scope`: the permission scopes
- `fetchMeetingInterval`: every this interval to fetch new meetings in MS team
- `checkMeetingInterval`: every this interval to check if any meetings ends and post messages
- `userName`: user name for the reporting apis
- `password`: password for the reporting apis
- `appRootUrl`: Root url of this application

There are also some configurations in: `manifest/manifest.json` file. Usually, you just need to modify:
- `id`: the id of the application, (you can use any uuid string)
- `bots.botId`: this id is the app(bot)'s id. Should be the same as `microsoftAppId` in the `config/default.js` file.

## Create the Bot in Microsoft portals
1. Visit page: https://apps.dev.microsoft.com/#/appList
   click "Add" to add an application.
   Input the application name and then click "Create".
   Copy the Application Id, and set it to the `microsoftAppId` configuration in the [config file](./config/default.js)

2. In the `Application Secrets` section, click button `Generate New Password`.
   Copy the generated password, and set it to the `microsoftAppPassword` configuration in the [config file](./config/default.js).


3. In the `Platforms` section, click "Add Platform" and choose **Web** platform.
   for the `Redirect URLs` property, set the uri:
   ```
        https://<HOST>/auth/microsoft/callback
   ```
   For example, if you are using ngrok, the Redirect URLs should be something like:
   ```
       https://32c97282.ngrok.io/auth/microsoft/callback
   ```

4. In the `Microsoft Graph Permissions` section, in `Delegated Permissions`, click the "Add" button, and add the following permissions:
   **User.Read**, **Group.Read.All**
5. Click "Save" to save the changes.

6. Visit page: https://dev.botframework.com/bots/new
7. the `Bot handle`, please fill in the app id (which created by step 1).
8. In the `Messaging endpoint`, please fill in the url like:
   ```
        https://<HOST>/api/messages
   ```
   For example, if you are using ngrok, the Redirect URLs should be something like:
   ```
       https://32c97282.ngrok.io/api/messages
   ```
9. In the `Paste your app ID below to continue`, please fill in the app id (which created by step 1)

10. Click the "Register" button to save the changes.
11. Go back to the bots list: https://dev.botframework.com/bots
    Click the bot you just created.
    In the `Add a featured channel`, click the icon of `Microsoft Teams`.

## Run the app locally

1. Deploy mongodb, and set the `mongodbURI` in the [config file](./config/default.js).
2. run ngrok to map the listen port to a public uri:
    ```
        ngrok http 3333
    ```
3. modify the configurations in `config/default.js`
4. run the following commands:
    ```
    npm i
    npm start
    ```
  Now the app should successfully running locally.


## Upload the app to MS team

1. Change the following configuration in `manifest/manifest.json` file:

    *id* - change to any UUID values, you can use this website to generate one: https://www.uuidgenerator.net/
    *bots.botId* - copy the value of `microsoftAppId` in `config/default.js`.
2. package the app with the following commands:
    ```
    npm install -g gulp
    gulp
    ```
    then you should see the `crush.zip` generated.
3.  Visit: https://teams.microsoft.com/  and login.
   Click the `Store` button in the left pannel, and go to the app store page.
4. Click `Upload a custom app` -> `Upload for culture works`, and select the file `crush.zip` to upload.
5. After the zip file is uploaded, you should see the newly uploaded app in that web page. Click the uploaded app,
   In the `Add to a team` section, select a team for the bot to install to. Then click the `Install` button.

## Subscribe in a Channel
Before you can get the CRUSH notification after a meeting ends, you have to setup/subscribe it in a channel. To do that, send a message in the channel:
```
@<your-bot> start
```
NOTE, <your-bot> is the bot name. that means you mention the bot and send it a 'start' command.
If you did not setup in the team before, you may get a reply like this:
> You did not authorize this bot yet. Please click [here]() and login with an admin account to authorize.

Then you click the link and login with an admin account().
After successfully authorized, you will get a reply:
>Congratulations! This channel is now monitoring the meetings.

Now, everything is done. If there are any meetings ends, you will get a CRUSH rating collection message like ![this](image/rating-card.png)


To stop subscription, send the message:
```
@<your-bot> stop
```


## Deploy to Azure (Optional)
Deploy to Azure is optional, because local deployment can verify all the features of the app.
If you want to deploy to azure, make sure you have a paid plan.

### Create Mongodb storage
1. Visit: https://portal.azure.com
2. Click "Azure Cosmos DB"
3. Click "Add"
4. Fill in the field: "API", choose "MongoDB".
5. Fill in other fields and click "create"
6. Click the newly created mongodb resource, and click "Add Collection" in the "Overview"
7. enter the database id and collection id (the collection id just named it anything).
8. Click the "Preview Features" and enable Mongodb aggregate pipeline.
9. In the "Settings" -> "Connection String", you can find the mongodb URI in `PRIMARY CONNECTION STRING`
   copy the mongodb URI to the configuration mongodbURI in the configuration file: `./config/default.js`

### Deploy the nodejs app
You can follow this instruction to zip/upload the current nodejs app:
https://docs.microsoft.com/en-us/azure/app-service/app-service-web-get-started-nodejs#create-a-project-zip-file
Make sure you changed the configurations in `./config/default` with correct url in:
 - oauth.authURL
 - oauth.callbackURL


## Verification

Please read [VERIFICATION.md](./VERIFICATION.md) file for details.