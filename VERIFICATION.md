# Culture Works CRUSH Rating - MS Teams Scheduled Meetings Bot PoC

This file is the verification for: Culture Works CRUSH Rating - MS Teams Scheduled Meetings Bot PoC.

## Working Deployment for easy testing

As the requirement ask us to, I have deploy a working deployment.
1. Sign-in page: https://portal.office.com/ with the following account:
```
account: hb_cw@culturespark.onmicrosoft.com
password: sp@rk200!
```
2. Click the `Store` button in the left pannel, and go to the app store page.
3. Click the "Culture Works", you can see an application named: `crush-bot-cloud`.
This app is a working deployment and can be use directly.
4. Click `crush-bot-cloud` and install it to the team you want to test in.
5. Now, you can go to any channels of the team to text the following message:
```
@sri-crush-bot start
```

Then it will ask you to authorized if you are not yet:
> You did not authorize this bot yet. Please click [here]() and login with an admin account to authorize.

After authorized (or you have been authorized, you can get a reply):
>Congratulations! This channel is now monitoring the meetings.

6. Now, you can add some meetings to the team, and after the meeting ends, you will get a message:
![card](image/rating-card.png)

7. Select the ratings and click `Rate`. Once rating recorder you will receive a message
> Thanks for the meeting `User's name`

8. To stop subscribing this message, you can type in the channel:
```
@sri-crush-bot stop
```
9. To test the report api's, import the postman collection present in `postman` folder.

10. Open the api to test, In authorization tab, select `Basic auth` and enter the user name and password. User name is `admin` and password is `password`.

11. api `/api/reports/meetingsRating` returns the list of meeting ratings and it support the following query parameters to support filter and pagination
- `meeting-id` - meeting unique id (iCalID from ms graph calendar api)
- `meeting-name` - meeting subject
- `team-id` - team unique uuid
- `team-name` - team name
- `page-no` - page no. If there is more than on page, response object would have `nextPage` value which should be given here for subsequent call to fetch the data.
- `size` - page size. Default value is 10. If there is more than on page, response object would have `size` value which should be given here for subsequent call to fetch the data.

12. api `/api/reports/meetingsStat` returns the count of meeting for each rating in aggregate and it supports the following query parameters to support filter
- `team-id` - team unique uuid
- `team-name` - team name

13. api `/api/reports/avgRating/:meetingId` returns the average rating of the meeting for the given meeting id in the resource path `:meetingId`

_**KNOWN ISSUE**_:
Because the app just need to authorize once, so if you use my working deployment, you may not see the message like:
> You did not authorize this bot yet. Please click [here]() and login with an admin account to authorize.

Because the app has been authorized. If you want to test the authorized workflow, you should deploy your own deployment.

## Verify with your deployment
Please read the `./README.md` to config/build the app, and upload to the MS team app store.
After doing that, the verification steps are the same as above.