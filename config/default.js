/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file is the configuration of the app.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

module.exports = {
  port: 3000,
  microsoftAppId: "02d7ba1e-8a6f-448b-a854-d8db26b99bc4",
  microsoftAppPassword: "lgbeAXEXRP3221+_kmkT0![",
  mongodbURI: "mongodb://crush:evcDpgRniLMkPKX60lhlqpLIXKMpjuJtiCR1fGw3kr1jK8hg6zKMbJsWIpeOmarUHrccFn7gMaNcvL7RbtRVLQ==@crush.documents.azure.com:10255/crush?ssl=true&replicaSet=globaldb",
  oauth: {
    authURL: "https://crush-app.azurewebsites.net/auth/microsoft",
    callbackURL: "https://crush-app.azurewebsites.net/auth/microsoft/callback",
    scope: [
      "Group.Read.All",
      "offline_access",
      "User.Read"
    ]
  },
  fetchMeetingInterval: 30, // every this interval to fetch new meetings in MS team
  checkMeetingInterval: 5, // every this interval to check if any meetings ends and post messages
  userName: 'admin', //reports api user name
  password: 'password', //reports api password
  appRootUrl: 'https://crush-app.azurewebsites.net' // deployed application's root endpoint <https://c9dd8d32.ngrok.io>
};