/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file defines the env variables of the configurations.
 * @author TCSDEVELOPER
 * @version 1.0
 */
module.exports = {
  port: "PORT",
  microsoftAppId: "MICROSOFT_APP_ID",
  microsoftAppPassword: "MICROSOFT_APP_PASSWORD",
  mongodbURI: "MONGODB_URI",
  oauth: {
    authURL: "OAUTH_AUTH_URL",
    callbackURL: "OAUTH_CALLBACK_URL"
  }
};
