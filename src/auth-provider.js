/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file is for providing the oauth 2 access tokens
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */


const passport = require('passport');
const refresh = require('passport-oauth2-refresh');
const config = require('config');
const Admin = require('./models').Admin;
const MicrosoftStrategy = require('passport-microsoft').Strategy;

const strategy = new MicrosoftStrategy({
  clientID: config.microsoftAppId,
  clientSecret: config.microsoftAppPassword,
  callbackURL: config.get('oauth').callbackURL
}, async function (accessToken, refreshToken, profile, done) {
  await Admin.remove({});
  // update the admin
  const admin = await Admin.create({
    userId: profile.id,
    accessToken,
    refreshToken,
    accessTokenCreatedAt: new Date()
  });

  return done(null, admin);
});
passport.use(strategy);

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});


refresh.use(strategy);

const setup = (app) => {
  app.use(passport.initialize());
  app.use(passport.session());

  app.get('/auth/microsoft',
    passport.authenticate('microsoft', {scope: config.get('oauth').scope}));

  app.get('/auth/microsoft/callback',
    passport.authenticate('microsoft', {failureRedirect: '/error'}),
    async function (req, res) {
      // Successful authentication, redirect home.
      res.redirect('/');
    });
};

const provider = async (cb) => {
  const admin = await Admin.findOne({});
  if (!admin) {
    return cb(new Error('the app is not authorized by admin yet.'));
  }

  if (new Date().getTime() - admin.accessTokenCreatedAt.getTime() < 60 * 60 * 1000) {
    // not necessary to refresh the token within 60 minutes
    return cb(null, admin.accessToken);
  }

  refresh.requestNewAccessToken('microsoft', admin.refreshToken, async function (err, accessToken, refreshToken) {
    if (err) {
      // cannot refresh the token, maybe the refresh token is expired
      await Admin.remove({});
      return cb(new Error('refresh token has been expired'));
    }

    admin.accessToken = accessToken;
    admin.refreshToken = refreshToken;
    admin.accessTokenCreatedAt = new Date();
    await admin.save();

    cb(null, accessToken);
  });
};


module.exports = {
  setup,
  provider
};