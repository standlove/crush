/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Represents the Rating model.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  meetingId: {type: String, required: true, index: true},
  teamId: String,
  teamName: String,
  subject: String,
  avgRating: Number,
  crushRating: [{
      rating: [String],
      userId: String, 
      name: String,
      date: { type: Date, default: Date.now }
  }]
});

module.exports = schema;