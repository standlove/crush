/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Represents the Admin model.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  userId: {type: String, required: true, index: true},
  accessToken: {type: String},
  refreshToken: {type: String},
  accessTokenCreatedAt: {type: Date}
});



module.exports = schema;