/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Represents the Meeting model.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  _id: {type: String, required: true},
  meetingId: String,
  groupId: {type: String, required: true, index: true},
  channelId: String,
  startsAt: {type: Date, required: true},
  endsAt: {type: Date, required: true},
  subject: {type: String},
  status: {type: String, enum: ['pending', 'done'], required: true, default: 'pending'},
  location: {type: Schema.Types.Mixed},
  attendees: [{name: String, address: String}]
}, {_id: false});



module.exports = schema;