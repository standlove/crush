/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Represents the Channel model.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  channelId: {type: String, required: true, index: true},
  groupId: {type: String},
  address: {type: Schema.Types.Mixed, required: true},
  status: {type: String, enum: ['active', 'inactive'], required: true},
  authorized: {type: Boolean, required: true},
  subscribesAt: {type: Date, required: true},
  teamName: String,
  channelName: String
});



module.exports = schema;