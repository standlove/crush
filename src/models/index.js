/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Initialize and exports all models.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */
const fs = require('fs');
const config = require('config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise || require('bluebird');
const conn = mongoose.createConnection(config.mongodbURI, {useNewUrlParser: true});
const models = {};

// Bootstrap models
fs.readdirSync(__dirname).forEach((file) => {
  if (file !== 'index.js') {
    const filename = file.split('.')[0];
    const schema = require(__dirname + '/' + filename);
    models[filename] = conn.model(filename, schema);
  }
});
module.exports = models;
