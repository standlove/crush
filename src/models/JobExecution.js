/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Represents the Job running status.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    lastExecutedTime: {type: Date, required: true}
});

module.exports = schema;