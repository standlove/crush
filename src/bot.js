/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file is for implementing the logics for the crush-bot.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

'use strict';
const builder = require('botbuilder');
const teams = require('botbuilder-teams');
const config = require('config');
const messageService = require('./message-service');
const SubscribedChannel = require('./models').SubscribedChannel;
const Rating = require('./models').Rating;
const Meeting = require('./models').Meeting;
const Admin = require('./models').Admin;
const util = require('./utils');
const _ = require('lodash');


/**
 * Starts the subscription.
 * @param session the session.
 * @returns {Promise<void>}
 */
const startSubscribe = async (session, connector) => {
  const channelId = util.getChannelId(session);

  if (!channelId) {
    return messageService.sendProactiveMessage(session.message.address,
      "This command can only apply in channel.");
  }

  let channel = await SubscribedChannel.findOne({ channelId });
  if (channel && channel.status === 'active') {
    if (!channel.authorized) {
      return messageService.sendProactiveMessage(session.message.address,
        `You did not authorize this bot yet. Please click [here](${config.get('oauth').authURL}?channelId=${channelId}) ` +
        "and login with an admin account to authorize.");
    }
    return messageService.sendProactiveMessage(session.message.address,
      "This channel has been started monitoring the meetings.");
  }
  const admin = await Admin.findOne({});
  if (!channel) {
    const teamInfo = await util.getTeamInfo(session, connector);
    const channelInfo = await util.getSubscribedChannelInfo(session, connector);
    channel = await SubscribedChannel.create({
      channelId,
      address: session.message.address,
      status: 'active',
      authorized: !!admin,
      subscribesAt: new Date(),
      groupId: teamInfo.aadGroupId,
      teamName: teamInfo.name,
      channelName: channelInfo.name
    });
  }

  if (!admin) {
    return messageService.sendProactiveMessage(session.message.address,
      `You did not authorize this bot yet. Please click [here](${config.get('oauth').authURL}?channelId=${channelId}) ` +
      "and login with an admin account to authorize.");
  }

  channel.status = 'active';
  channel.authorized = true;
  channel.subscribesAt = new Date();
  await channel.save();

  return messageService.sendProactiveMessage(session.message.address,
    "Congratulations! This channel is now monitoring the meetings.");

};

/**
 * Stops the subscription.
 * @param session the session.
 * @returns {Promise<void>}
 */
const stopSubscribe = async (session) => {
  const channelId = util.getChannelId(session);
  let channel = await SubscribedChannel.findOne({ channelId });
  if (channel) {
    channel.status = 'inactive';
    await channel.save();
  }
  return messageService.sendProactiveMessage(session.message.address,
    "Now the bot has stopped monitoring the meetings in this channel.");
};

/**
 * Creates or Updates the rating document with the value provided by user.
 *
 * @param {*} session the session.
 * @param {*} ratingData the rating data.
 * @param {*} user the user object.
 * @returns {Void}
 */
const updateMeetingRating = async (session, ratingData, user) => {
  let meeting = await Rating.findOne({ meetingId: ratingData.meetingId });
  let crush = [];
  if (ratingData.crushRating.trim().length > 0) {
    if (ratingData.crushRating.indexOf(';') > -1) {
      // mobile flow has ; as delimiter
      crush = ratingData.crushRating.split(';');
    } else {
      //web flow has , as delimiter
      crush = ratingData.crushRating.split(',');
    }
  }
  if (meeting) {

    if (meeting.crushRating) {
      const userRating = _.find(meeting.crushRating, (it) => it.userId == user.aadObjectId);
      if (userRating) {
        //double click scenario. Ignore the second time rating from the same user.
        console.log('Rating already updated.')
        return `${user.name}, We ignored this response, since we had your rating already!`;
      }
    }
    // update the user rating
    meeting.crushRating.push({
      rating: crush,
      userId: user.aadObjectId,
      name: user.name
    });
    let ratingSum = 0;
    for (let c of meeting.crushRating) {
      ratingSum = ratingSum + c.rating.length
    }
    const avgRating = ratingSum / meeting.crushRating.length;
    console.log(avgRating);
    meeting.avgRating = util.roundRating(avgRating);
    meeting.save();
  } else {
    //It's a first rating, create a rating object
    const mtg = await Meeting.findOne({meetingId: ratingData.meetingId});
    const channel = await SubscribedChannel.findOne({ channelId: util.getChannelId(session) });
    meeting = await Rating.create({
      meetingId: ratingData.meetingId,
      avgRating: crush.length,
      teamId: channel.groupId,
      teamName: channel.teamName,
      subject: mtg.subject,
      channelName: channel.channelName,
      crushRating: [{
        rating: crush,
        userId: user.aadObjectId,
        name: user.name
      }]
    });
  }
  return `Thanks for the rating ${user.name}`;
}

module.exports.setup = function (app) {

  // Create a connector to handle the conversations
  const connector = new teams.TeamsChatConnector({
    appId: config.microsoftAppId,
    appPassword: config.microsoftAppPassword
  });

  // Define a simple bot with the above connector that echoes what it received
  const bot = new builder.UniversalBot(connector, async function (session) {
    const text = teams.TeamsMessage.getTextWithoutMentions(session.message);
    if (session.message.value) {
      // rating submission received
      const resp = await updateMeetingRating(session, session.message.value, session.message.user);
      session.send(resp);
    }
    if (text.toLowerCase() === "start") {
      await startSubscribe(session, connector);
    } else if (text.toLowerCase() === "stop") {
      await stopSubscribe(session);
    } else if (!text || text.toLowerCase() === "thanks for the rating.") {
      // do nothing. This is the card acknowledgement case.
    } else {
      console.log(text.toLowerCase());
      return messageService.sendProactiveMessage(session.message.address,
        "I don't know what you mean. You can text me with **start** or **stop** to start/stop monitoring the meetings in this channel.");
    }
  });
  bot.set('storage', new builder.MemoryBotStorage());


  // Setup an endpoint on the router for the bot to listen.
  // NOTE: This endpoint cannot be changed and must be api/messages
  app.post('/api/messages', connector.listen());

  // Export the connector for any downstream integration - e.g. registering a messaging extension
  module.exports.connector = connector;
  module.exports.bot = bot;
};
