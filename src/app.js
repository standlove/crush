/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Represents the bootstrap of the app.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */


'use strict';
const path = require('path');
const express = require('express');
const config = require('config');
const app = express();
const cookieParser = require('cookie-parser');
const authProvider = require('./auth-provider');
const messageService = require('./message-service');
const fetchMeetings = require('./jobs/fetchMeetings');
const checkMeetings = require('./jobs/checkMeetings');
const reports = require('./apis/reports');

app.use(cookieParser());

//setting up reports api
reports.setup(app);

// Adding a bot to our app
const bot = require('./bot');
bot.setup(app);



authProvider.setup(app);
messageService.setup(bot.bot);

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

// public folder to serve the image
app.use('/static', express.static(path.join(__dirname, 'public')))

// Setup home page
app.get('/', function (req, res) {
  res.render('success');
});

app.get('/error', function (req, res) {
  res.render('error', {authURL: config.get('oauth').callbackURL});
});

// Start our nodejs app
app.listen(config.port, function () {
  console.log(`App started listening on port ${config.port}`);
});

setInterval(fetchMeetings, config.fetchMeetingInterval * 1000);
setInterval(checkMeetings, config.checkMeetingInterval * 1000);
