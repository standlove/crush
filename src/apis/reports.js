/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * Represents the reports api of the app.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */
const Rating = require('../models/index').Rating;
const _ = require('lodash');
const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const config = require('config');

/**
 * Initialize the report api.
 *
 * @param {Object} app
 */
module.exports.setup = (app) => {

    //Basic http security
    passport.use(new BasicStrategy((userId, password, done) => {
        if (userId == config.userName && password == config.password) {
            return done(null, 'admin');
        } else {
            return done(null, false);
        }
    }));

    // setting up password basic authentication middleware
    app.use('/api/reports/', passport.authenticate('basic', { session: false }));

    // api to get the meetings rating
    app.get('/api/reports/meetingsRating', async (req, res) => {
        const filter = getMeetingRatingFilter(req);
        // pagination
        const pageNo = req.query['page-no'] && /^\d+$/.test(req.query['page-no'])? parseInt(req.query['page-no']) : 1;
        const size = req.query.size && /^\d+$/.test(req.query.size) ? parseInt(req.query.size) : 10;

        const ratings = await Rating.find(filter, { _id: 0, __v: 0, "crushRating._id": 0 }, { skip: (pageNo - 1) * size, limit: size + 1 });
        let data;
        // if there is more records then page size, add the nextPage parameters in response
        if (ratings.length > size) {
            ratings.pop();
            data = {
                nextPage: pageNo + 1,
                pageSize: size,
                data: ratings
            }
        } else {
            data = {
                data: ratings
            }
        }
        res.json(data);
    });

    // api to get the meeting stat.
    app.get('/api/reports/meetingsStat', async (req, res) => {
        //filtering conditions
        const filter = getMeetingStatFilter(req);
        const ratings = await Rating.aggregate([
            filter,
            { $group: { _id: "$avgRating", noOfMeetings: { $sum: 1 } } },
            { $project: { avgRating: "$_id", noOfMeetings: "$noOfMeetings", _id: 0 } }
        ]);
        res.json(ratings);
    });

    // api to get the average rating of the meeting.
    app.get('/api/reports/avgRating/:meetingId', async (req, res) => {

        const meeting = await Rating.findOne({ meetingId: req.params.meetingId })
            .select({ _id: 0, meetingId: 1, avgRating: 1 });
        if (meeting) {
            res.json(meeting);
        } else {
            // send not found message if the meeting object is not found.
            res.status(404).json({
                error: {
                    msg: `Resource not found for the meetingId ${req.params.meetingId}`
                }
            })
        }

    });
}


/**
 *  Helper function to get the meeting status filter criteria.
 *
 * @param {Object} req
 * @returns {Object} filter
 */
function getMeetingStatFilter(req) {
    const filter = { $match: {} };
    if (req.query['team-id']) {
        filter.$match.teamId = req.query['team-id'];
    }
    if (req.query['team-name']) {
        filter.$match.teamName = req.query['team-name'];
    }
    return filter;
}

/**
 * Helper function to get the Meeting rating filter criteria.
 *
 * @param {Object} req
 * @returns {Object} filter
 */
function getMeetingRatingFilter(req) {
    const filter = {};
    if (req.query['meeting-id']) {
        filter.meetingId = req.query['meeting-id'];
    }
    if (req.query['meeting-name']) {
        filter.subject = req.query['meeting-name'];
    }
    if (req.query['team-id']) {
        filter.teamId = req.query['team-id'];
    }
    if (req.query['team-name']) {
        filter.teamName = req.query['team-name'];
    }
    return filter;
}
