/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file is a wrapper for microsoft graph api.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const MicrosoftGraph = require("@microsoft/microsoft-graph-client");
const authProvider = require('./auth-provider');

module.exports = MicrosoftGraph.Client.init({
  authProvider: authProvider.provider
});
