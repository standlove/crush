/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file is for sending channel messages
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */


const _ = require('lodash');
const builder = require('botbuilder');
const crushCard = require('./card');


const SubscribedChannel = require('./models').SubscribedChannel;

let messageBot = null;

/**
 * Sets up the message service.
 * @param bot the bot
 */
const setup = (bot) => {
  messageBot = bot;
};

/**
 * Sends a message as a reply to the original message.
 * @param channelId the channel id.
 * @param msgContent the message content.
 * @returns {Promise<void>}
 */
const sendMessageAsReply = async (channelId, msgContent) => {
  const address = await __findChannelAddress(channelId);
  sendProactiveMessage(address, msgContent);
};

/**
 * Sends a message.
 * @param channelId the channel id.
 * @param msgContent the msg content.
 * @returns {Promise<void>}
 */
const sendMessage = async (channelId, msgContent) => {
  const address = await __findChannelAddress(channelId);
  const newAddress = _.clone(address, true);
  newAddress.conversation.id = newAddress.conversation.id.replace(/;.*$/g, '');
  sendProactiveMessage(newAddress, msgContent);
};

/**
 * Helper method to find the channel conversation address.
 * @param channelId the channel id.
 * @returns {Promise<*>}
 * @private
 */
const __findChannelAddress = async (channelId) => {
  const channel = await SubscribedChannel.findOne({channelId});
  if (!channel) {
    throw new Error(`this channel: [${channelId}] is not subscribed. `);
  }
  return channel.address;
};

/**
 * Sends the message
 * @param address the address to send.
 * @param msgContent the message content.
 */
const sendProactiveMessage = (address, msgContent) => {
  const msg = new builder.Message().address(address);
  msg.text(msgContent);
  msg.textFormat('markdown');
  messageBot.send(msg);
};

const sendCrushRatingCard = async (channelId, meeting) => {
  const address = await __findChannelAddress(channelId);
  const newAddress = _.clone(address, true);
  newAddress.conversation.id = newAddress.conversation.id.replace(/;.*$/g, '');
  const msg = new builder.Message().address(address);
  const card = crushCard.getCrushCard(meeting);
  msg.addAttachment(card);
  messageBot.send(msg);
};

module.exports = {
  setup,
  sendMessageAsReply,
  sendMessage,
  sendProactiveMessage,
  sendCrushRatingCard
};