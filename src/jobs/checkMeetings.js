/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file checks the meeting status
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const SubscribedChannel = require('../models').SubscribedChannel;
const Meeting = require('../models').Meeting;
const messageService = require('../message-service');
const checkChannels = require('./checkChannels');

module.exports = async () => {
  // find the pending ended meetings
  await checkChannels();

  const meetings = await Meeting.find({status: 'pending', endsAt: {$lte: new Date()}});
  for (let m of meetings) {
    // find the channels of the group
    let channels = await SubscribedChannel.find({channelId: m.channelId, status: 'active', subscribesAt: {$lte: m.endsAt}});
    // send the messages to the channels
    for (let c of channels) {
      await messageService.sendCrushRatingCard(c.channelId, m);
    }

    // mark the meeting status as done
    m.status = 'done';
    await m.save();
  }
};