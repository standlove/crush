/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file fetches the meetings.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const SubscribedChannel = require('../models').SubscribedChannel;
const Meeting = require('../models').Meeting;
const Execution = require('../models').JobExecution;
const client = require('../graph-client');
const _ = require('lodash');

/**
 * Helper function to get the next page of the api
 *
 * @param {String} url resource url for the next page
 * @returns {Promise<any>} the next page data
 */
const getNextPage = (url) => {
  return new Promise((resolve, reject) => {
    client.api(url).get((err, res) => {
      if (err) {
        reject(err);
      } else {
        resolve(res);
      }
    })
  })
}

/**
 * Helper function to fetch the meetings of a group first time.
 *
 * @param {*} groupId the id of the group.
 * @param {*} startTime start time
 * @param {*} endTime end time
 * @returns {Promise<any>} the meetings.
 */
const initialFetchMeetingsForGroup = (groupId, startTime, endTime) => {
  return new Promise((resolve, reject) => {
    client.api(`/groups/${groupId}/calendar/calendarView?startDateTime=${startTime.toISOString()}&endDateTime=${endTime.toISOString()}`).get((err, result) => {
      if (err) {
        return reject(err);
      }
      return resolve(result);
    })
  });
};

/**
 * Fetches the meetings of a group (team) by iterating all the pages..
 *
 * @param {*} groupId the id of the group.
 * @param {*} startTime start time
 * @param {*} endTime end time
 * @returns {Promise<any>} the meetings.
 */
const fetchMeetingsForGroup = async (groupId, startTime, endTime) => {
  const data = await initialFetchMeetingsForGroup(groupId, startTime, endTime);
  let nextPageUrl = data['@odata.nextLink'];
  let response = data.value;
  // loop till the nextPage url not found in the response
  while (nextPageUrl) {
    const nextPage = await getNextPage(nextPageUrl);
    nextPageUrl = nextPage['@odata.nextLink'];
    response = _.concat(response, nextPage.value);
  }
  return response;
}

module.exports = async () => {
  const channels = await SubscribedChannel.find({ status: 'active', authorized: true });
  const job = await Execution.findOne({});
  const endTime = new Date();
  const groupsSet = {};
  channels.forEach(c => {
    groupsSet[c.groupId] = true;
  });
  // fetch the meetings for each group
  for (let groupId in groupsSet) {
    // fetch the meeting created after last execution of job till now.
    // if last execution time is not available it's a first time so use channel subscribed time.
    
    const startTime = job ? job.lastExecutedTime : channel.subscribesAt;
    let meetings = await fetchMeetingsForGroup(groupId, startTime, endTime);
    meetings = meetings.filter(m => m.start && m.end).map(m => {
      const channelName = m.location.displayName.split('/')[1].trim();
      const channel = _.filter(channels, (c) => {
        return c.groupId == groupId && c.channelName == channelName;
      })
      return {
        _id: m.id,
        meetingId: m.iCalUId,
        groupId,
        channelId: channel.length > 0 ? channel[0].channelId : null,
        subject: m.subject,
        startsAt: new Date(m.start.dateTime.replace(/0000$/g, 'Z')),
        endsAt: new Date(m.end.dateTime.replace(/0000$/g, 'Z')),
        attendees: _.map(m.attendees, it => it.emailAddress),
        location: m.location
      }
    }).filter(m => m.channelId);

    for (let m of meetings) {
      await Meeting.update({ _id: m._id }, m, { upsert: true, setDefaultsOnInsert: true });
    }
  }
  if (job) {
    // update the job execution time
    job.lastExecutedTime = endTime;
    await job.save();
  } else {
    // create the job execution time
    await Execution.create({
      lastExecutedTime: endTime
    });
  }
};

module.exports.fetchMeetingsForGroup = fetchMeetingsForGroup;