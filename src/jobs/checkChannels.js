/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file checks the channel status
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */


const SubscribedChannel = require('../models').SubscribedChannel;
const Admin = require('../models').Admin;
const messageService = require('../message-service');
const utils = require('../utils');

module.exports = async () => {
  const admin = await Admin.findOne({});
  if (!admin) {
    // mark all unauthorized
    return await SubscribedChannel.update({}, {$set: {authorized: false}}, {multi: true});
  }
  const channels = await SubscribedChannel.find({authorized: false});
  for (let channel of channels) {
    channel.authorized = true;
    if (!channel.groupId) {
      channel.groupId = await utils.detectTeamGroupId(channel.channelId);
    }
    await channel.save();
    await messageService.sendMessageAsReply(channel.channelId,
      "Congratulations! This channel is now monitoring the meetings.");
  }
};