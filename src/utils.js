/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file provides some helper methods.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */


const client = require('./graph-client');
const _ = require('lodash');
/**
 * Lists all the groups of the user.
 * @returns {Promise<any>} the groups
 */
function listGroups() {
  return new Promise((resolve, reject) => {
    client.api('/groups').get((err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.value);
      }
    })
  });
}

/**
 * List all the channels from a group.
 * @param groupId the id of the group.
 * @returns {Promise<any>} the channels.
 */
function listChannels(groupId) {
  return new Promise((resolve, reject) => {
    client.api(`/teams/${groupId}/channels`).version('beta').get((err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result.value);
      }
    })
  });
}

/**
 * Detect which team(group) the channel belongs to.
 * @param channelId the id of the channel.
 * @returns {Promise<*>} the group id
 */
async function detectTeamGroupId(channelId) {
  const groups = await listGroups();
  for (let i = 0; i < groups.length; i++) {
    try {
      const channels = await listChannels(groups[i].id);
      const channel = channels.filter(c => c.id === channelId).shift();
      if (channel) {
        return groups[i].id;
      }
    } catch (e) {
      // ignore
    }
  }
  return null;
}

/**
 * Helper function to round the value to it's nearest 0.5.
 *
 * @param {Number} value the give value to round.
 * @returns {Number} rounded value.
 */
function roundRating(value) {
  return Math.round(value * 2) / 2;
}
/**
 * Gets the channel id.
 * @param session the session.
 * @returns {*} the channel id.
 */
function getChannelId(session) {
  if (session.message.sourceEvent.channel) {
    return session.message.sourceEvent.channel.id;
  }
  return null;
}
/**
 * Helper function to get the team id.
 *
 * @param {Object} session the session.
 * @returns {String} the team id.
 */
function getTeamId(session) {
  if (session.message.sourceEvent.team) {
    return session.message.sourceEvent.team.id;
  }
  return null;
};
/**
 * Helper function to get the service url.
 *
 * @param {Object} session the session.
 * @returns {String} the service url.
 */
function getServiceUrl(session) {
  if (session.message.address.serviceUrl) {
    return session.message.address.serviceUrl;
  }
  return null;
};
/**
 * Helper function to get the team information.
 *
 * @param {Object} session the session.
 * @param {Object} connector the connector.
 * @returns {Object} the team info object.
 */
function getTeamInfo(session, connector) {
  return new Promise((resolve, reject) => {
    connector.fetchTeamInfo(getServiceUrl(session), getTeamId(session), (err, res) => {
      if (err) {
        reject(err);
      } else {
        resolve(res);
      }
    });
  })
}
/**
 * Helper function to get the channel information.
 *
 * @param {Object} session the session.
 * @param {Object} connector the connector.
 * @returns {Object} the channel info object.
 */
function getSubscribedChannelInfo(session, connector) {
  return new Promise((resolve, reject) => {
    connector.fetchChannelList(getServiceUrl(session), getTeamId(session), (err, res) => {
      if (err) {
        reject(err);
      } else {
        const channel = _.filter(res, (it) => (it.id === getChannelId(session)));
        if (!channel[0].name) {
          channel[0].name = 'General';
        }
        resolve(channel[0]);
      }
    })
  })
}
module.exports = {
  detectTeamGroupId,
  roundRating,
  getTeamInfo,
  getSubscribedChannelInfo,
  getServiceUrl,
  getTeamId,
  getChannelId
};