/**
 * Copyright (c) 2018 TopCoder, Inc. All rights reserved.
 */

/**
 * This file is for defining the crush rating adaptive card.
 *
 * @author TCSDEVELOPER
 * @version 1.0
 */

const _ = require('lodash');
const config = require('config');
/**
 * Defines the CRUSH rating adaptive card.
 *
 * @param {Object} meeting the meeting.
 * @returns {Object} CRUSH rating card.
 */
module.exports.getCrushCard = (meeting) => {
    // set the attendees data
    const attendees = _.map(meeting.attendees, (m) => {
        return {
            "type": "TextBlock",
            "text": m.name,
            "spacing": "none"
        }
    });
    // set the crush image
    const crushImg = [{
        "type": "Image",
        "url": `${config.appRootUrl}/static/crush.jpg`,
        "size": "auto",
        "separator": true
    },
    {
        "type": "TextBlock",
        "text": "Please provide the CRUSH rating,",
    }
];
    const meetingInfo = [{
        "type": "TextBlock",
        "text": meeting.subject,
        "weight": "bolder",
        "size": "large"
    },
    {
        "type": "TextBlock",
        "text": `${meeting.startsAt.toString()} - ${meeting.endsAt.toString()}`,
        "isSubtle": true,
        "spacing": "none"
    },
    {
        "type": "TextBlock",
        "text": "**Attendees**",
        "separator": true
    }];
    const ratingForm = [{
        "type": "Input.ChoiceSet",
        "id": "crushRating",
        "isMultiSelect": true,
        "style": "expanded",
        "choices": [
            {
                "title": "Camera always on",
                "value": "C"
            },
            {
                "title": "Respect the clock",
                "value": "R"
            },
            {
                "title": "Understand attendee count",
                "value": "U"
            },
            {
                "title": "Set expectations",
                "value": "S"
            },
            {
                "title": "Hearing",
                "value": "H"
            }
        ]
    }]
    const body = _.concat(meetingInfo, attendees, crushImg, ratingForm);
    return card = {
        'contentType': 'application/vnd.microsoft.card.adaptive',
        'content': {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "type": "AdaptiveCard",
            "version": "1.0",
            "body": body,
            "actions": [
                {
                    "type": "Action.Submit",
                    "title": "Rate",
                    "data": {
                        "meetingId": meeting.meetingId
                    }
                }
            ]
        }
    }
}