var gulp = require('gulp');
var zip = require('gulp-zip');

gulp.task('generate-manifest', function() {
    gulp.src(['manifest/*'])
        .pipe(zip('crush.zip'))
        .pipe(gulp.dest(__dirname));
});

gulp.task('default', ['generate-manifest'], function() {
    console.log('Build completed. Output in manifest folder');
});